<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'type_id', 'password', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function type()
    {
        return $this->hasOne('App\Type', 'id', 'type_id');
    }

    public function user_access()
    {
        return $this->hasMany('App\User_access', 'user_id', 'id');
    }

    public function updated_who()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

    public function created_who()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
