<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AccountRequest;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Access;
use App\Type;
use App\User_access;
use Auth;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'data' => User::where('id', Auth::user()->id)->first(),
            'user_types' => Type::all(), 
            'access' => Access::all(),
            'user_access' => User_access::where('user_id', Auth::user()->id)->get()
        ];

        return view('account.account', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountRequest $request)
    {
        $access = $request['access'];
        $type = $request->user_type;
        $password = Hash::make($request->password);
        $request_f = $request->except(['_token', 'user_type', 'access', 'password', 'password_confirmation']);

        if(Auth::user()->is_super_admin == 1)
            $request_f = $request->except(['_token', 'status', 'user_type', 'access', 'password', 'password_confirmation']);
        
        $to_update = ['type_id' => $type, 'updated_by' => Auth::user()->id];

        if(Auth::user()->is_super_admin == 1)
            $to_update = ['updated_by' => Auth::user()->id ];

        if(!empty($request->password) or !empty($request->password_confirmation))
            $to_update['password'] = $password;

        $user = User::where('id', Auth::user()->id)->update(array_merge($request_f, $to_update));

        if(Auth::user()->is_super_admin == 0)
        {
            User_access::where('user_id', Auth::user()->id)->delete();

            foreach($access as $m)
            {
                $data = [
                    'user_id' => Auth::user()->id,
                    'access_id' =>  $m,
                    'created_by' =>  Auth::user()->id
                ];
                
                User_access::create($data);
            }
        }

        return redirect('account')->with('status', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
