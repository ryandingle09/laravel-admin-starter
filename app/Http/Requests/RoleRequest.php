<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Role;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $check = Role::where('id', $request->id)->first();
        $unique = (isset($check->prefix) && $check->prefix == $request->prefix) ? '' : '|unique:roles';

        return [
            'name' => 'required|max:255',
            'modules' => 'required',
            'prefix' => 'required'.$unique.'|max:255'
        ];
    }
}
