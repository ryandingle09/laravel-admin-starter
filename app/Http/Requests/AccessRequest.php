<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\Access;

class AccessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $check = Access::where('id', $request->id)->first();
        $unique = (isset($check->prefix) && $check->prefix == $request->prefix) ? '' : '|unique:access';

        return [
            'name' => 'required|max:255',
            'prefix' => 'required'.$unique.'|max:255'
        ];
    }
}
