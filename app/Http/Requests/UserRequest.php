<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use App\User;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $check = User::where('id', $request->id)->first();
        $unique = (isset($check->email) && $check->email == $request->email) ? '' : '|unique:users';
        
        $validate = '';

        if(!empty($request->password) or !empty($request->password_confirmation))
            $validate = 'required|confirmed|string|min:8';

        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'access' => 'required',
            'user_type' => 'required',
            'status' => 'required',
            'email' => 'required|string|email|max:255'.$unique.'',
            'password' => ''.$validate.'',
        ];
    }
}
