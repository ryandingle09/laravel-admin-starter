<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role_module extends Model
{
    protected $fillable = ['role_id', 'module_id', 'created_by', 'updated_by'];

    public function modules()
    {
        return $this->hasMany('App\Module', 'id', 'module_id');
    }
}
