<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = ['user_id', 'module_id', 'action', 'new_data', 'old_data'];

    public function access()
    {
        return $this->hasOne('App\Access', 'id', 'user_id');
    }

    public function module()
    {
        return $this->hasOne('App\Module', 'id', 'module_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
