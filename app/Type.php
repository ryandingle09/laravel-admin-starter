<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = ['name', 'prefix', 'description', 'role_id', 'created_by', 'updated_by'];

    public function role()
    {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    public function updated_who()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

    public function created_who()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
