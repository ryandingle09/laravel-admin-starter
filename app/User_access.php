<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_access extends Model
{
    protected $table = 'user_access';

    protected $fillable = ['user_id', 'access_id', 'created_by', 'updated_by'];

    public function access()
    {
        return $this->hasOne('App\Access', 'id', 'access_id');
    }
}
