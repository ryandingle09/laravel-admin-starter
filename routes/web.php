<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Not authenticated routes
Route::get('/', 'HomeController@index')->name('home');
Auth::routes(['register' => false]);
// end --

// Authenticated routes
Route::group(['middleware' => 'auth'], function(){

    // dashboard module --

    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    // end dashboard


    // account module --

    Route::get('/account', 'AccountController@index')->name('account');
    Route::post('/account', 'AccountController@update')->name('account-update');

    // end account

    // settings module (users, user roles, user types, user access level, modules) --

    # Users
    Route::get('/users', 'UserController@index')->name('user');
    Route::get('/users/create', 'UserController@create')->name('user-create');
    Route::post('/users/store', 'UserController@store')->name('user-store');
    Route::get('/users/{id}/show', 'UserController@show')->name('user-show');
    Route::get('/users/{id}/edit', 'UserController@edit')->name('user-edit');
    Route::post('/users/{id}/update', 'UserController@update')->name('user-update');
    Route::post('/users/{id}/destroy', 'UserController@destroy')->name('user-destroy');

    # Users Roles
    Route::get('/roles', 'RoleController@index')->name('role');
    Route::get('/roles/create', 'RoleController@create')->name('role-create');
    Route::post('/roles/store', 'RoleController@store')->name('role-store');
    Route::get('/roles/{id}/show', 'RoleController@show')->name('role-show');
    Route::get('/roles/{id}/edit', 'RoleController@edit')->name('role-edit');
    Route::post('/roles/{id}//update', 'RoleController@update')->name('role-update');
    Route::post('/roles/{id}/destroy', 'RoleController@destroy')->name('role-destroy');

    # Users Types
    Route::get('/types', 'TypeController@index')->name('type');
    Route::get('/types/create', 'TypeController@create')->name('type-create');
    Route::post('/types/store', 'TypeController@store')->name('type-store');
    Route::get('/types/{id}/show', 'TypeController@show')->name('type-show');
    Route::get('/types/{id}/edit', 'TypeController@edit')->name('type-edit');
    Route::post('/types/{id}/update', 'TypeController@update')->name('type-update');
    Route::post('/types/{id}/destroy', 'TypeController@destroy')->name('type-destroy');

    # Users Access
    Route::get('/access', 'AccessController@index')->name('access');
    Route::get('/access/create', 'AccessController@create')->name('access-create');
    Route::post('/access/store', 'AccessController@store')->name('access-store');
    Route::get('/access/{id}/show', 'AccessController@show')->name('access-show');
    Route::get('/access/{id}/edit', 'AccessController@edit')->name('access-edit');
    Route::post('/access/{id}/update', 'AccessController@update')->name('access-update');
    Route::post('/access/{id}/destroy', 'AccessController@destroy')->name('access-destroy');

    # Modules
    Route::get('/modules', 'ModuleController@index')->name('module');
    Route::get('/modules/create', 'ModuleController@create')->name('module-create');
    Route::post('/modules/store', 'ModuleController@store')->name('module-store');
    Route::get('/modules/{id}/show', 'ModuleController@show')->name('module-show');
    Route::get('/modules/{id}/edit', 'ModuleController@edit')->name('module-edit');
    Route::post('/modules/{id}/update', 'ModuleController@update')->name('module-update');
    Route::post('/modules/{id}/destroy', 'ModuleController@destroy')->name('module-destroy');

    # Logging/Audit Trail
    Route::get('/logs', 'LogController@index')->name('log');
    Route::get('/logs/{id}/show', 'LogController@show')->name('log-show');

    // end settings
});
// end --
