<!-- Sidebar -->
<?php
    $route = Route::currentRouteName();
    $list = 'user user-create user-edit user-show role role-create role-edit role-show type type-create type-edit access access-create access-show access-edit module module-create module-edit module-show';
?>

<div class="sidebar sidebar-style-2">			
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ asset('app/img/profile.jpg') }}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            {{ ucfirst(Auth::user()->first_name) }} {{ ucfirst(Auth::user()->last_name) }}
                            <span class="user-level">{{ (Auth::user()->type) ? Auth::user()->type->role->name : 'Super Admin'}}</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="{{ route('account') }}">
                                    <span class="link-collapse">Account Settings</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item {{ (Route::currentRouteName() == 'dashboard') ? 'active' : '' }}">
                    <a href="{{ route('dashboard') }}">
                        <i class="fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <?php 
                    
                    $uam = [];

                    if(Auth::user()->is_super_admin == 0):

                        foreach(Auth::user()->type->role->role_modules as $um):
                            foreach($um->modules as $m):
                                $uam[] = $m->prefix;
                            endforeach;
                        endforeach;

                    endif;
                    
                    foreach(\App\Module::orderBy('order','asc')->get() as $item):
                        if(Auth::user()->is_super_admin == 0):
                            foreach($uam as $a):
                                if($a == $item->prefix):
                ?>
                                    <li class="nav-item {{ (strpos(''.$item->prefix.' '.$item->prefix.'-create '.$item->prefix.'-edit '.$item->prefix.'-show', $route) !== false) ? 'active' : '' }}">
                                        <a href="{{ route($item->prefix) }}">
                                            <i class="{{$item->icon}}"></i>
                                            <p>{{$item->name}}</p>
                                        </a>
                                    </li>
                <?php
                                endif;
                            endforeach;
                        else:
                ?>
                            <li class="nav-item {{ (strpos(''.$item->prefix.' '.$item->prefix.'-create '.$item->prefix.'-edit '.$item->prefix.'-show', $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route($item->prefix) }}">
                                    <i class="{{$item->icon}}"></i>
                                    <p>{{$item->name}}</p>
                                </a>
                            </li>
                <?php
                        endif;
                    endforeach;
                ?>

                <!--  -->

                <!-- <li class="nav-item {{ (strpos($list, $route) !== false) ? 'active submenu' : '' }}">
                    <a data-toggle="collapse" href="#sidebarLayouts" aria-expanded="{{ (strpos($list, $route) !== false) ? 'true' : '' }}">
                        <i class="fas fa-cog"></i>
                        <p>Settings</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse {{ (strpos($list, $route) !== false) ? 'show' : '' }}" id="sidebarLayouts">
                        <ul class="nav nav-collapse">
                            <li class="{{ (strpos('user user-create user-edit', $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route('user') }}" class="active">
                                    <span class="sub-item">
                                        Users 
                                    </span>
                                </a>
                            </li>
                            <li class="{{ (strpos('type type-create type-edit type-show', $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route('type') }}">
                                    <span class="sub-item">User Type</span>
                                </a>
                            </li>
                            <li class="{{ (strpos('role role-create role-edit role-show', $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route('role') }}">
                                    <span class="sub-item">Roles</span>
                                </a>
                            </li>
                            <li class="{{ (strpos('access access-create access-edit access-show', $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route('access') }}">
                                    <span class="sub-item">Access Rights</span>
                                </a>
                            </li>
                            <li class="{{ (strpos('module module-create module-edit module-show', $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route('module') }}">
                                    <i class="fas fa-eye"></i>
                                    <p>Modules<p>
                                </a>
                            </li>
                            <li class="nav-item {{ (strpos('log log-show', $route) !== false) ? 'active' : '' }}">
                                <a href="{{ route('log') }}">
                                    <i class="fas fa-eye"></i>
                                    <p>Logs</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li> -->
            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->