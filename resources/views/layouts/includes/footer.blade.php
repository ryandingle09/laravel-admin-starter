
<footer class="footer" style="{{ (!Auth::check()) ? '
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 60px;
    background-color: #f5f5f5;
' : '' }}">
    <div class="container-fluid">
        <nav class="pull-left">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="/">
                        {{ config('app.name') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        Help
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        Licenses
                    </a>
                </li>
            </ul>
        </nav>
        <div class="copyright ml-auto">
            {{ date('Y') }}, made with <i class="fa fa-heart heart text-danger"></i> by <a href="/">{{ config('app.name') }}</a>
        </div>				
    </div>
</footer>