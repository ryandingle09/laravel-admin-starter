<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{ asset('app/img/icon.ico') }}" type="image/x-icon"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->

    <!-- Fonts and icons -->
	<script src="{{ asset('app/js/plugin/webfont/webfont.min.js') }}"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ["{{ asset('app/css/fonts.min.css') }}"]},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	
    <link rel="stylesheet" href="{{ asset('app/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('app/css/atlantis.min.css') }}"> 
   

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{ asset('app/css/demo.css') }}">
</head>
<body>
    <div class="wrapper">
        @include('layouts.includes.header')
        
        @if(Auth::check())
            @include('layouts.includes.sidebar')
        @endif

        @yield('content')
        
        @if(!Auth::check())
        @include('layouts.includes.footer')
        @endif
    </div>
    
    <!--   Core JS Files   -->

    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    <script src="{{ asset('app/js/core/jquery.3.2.1.min.js') }}"></script>
    <script src="{{ asset('app/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('app/js/core/bootstrap.min.js') }}"></script>

    <!-- jQuery UI -->
    <script src="{{ asset('app/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('app/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js') }}"></script>

    <!-- jQuery Scrollbar -->
    <script src="{{ asset('app/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js') }}"></script>


    <!-- Chart JS -->
    <!-- <script src="{{ asset('app/js/plugin/chart.js/chart.min.js') }}"></script> -->

    <!-- jQuery Sparkline -->
    <!-- <script src="{{ asset('app/js/plugin/jquery.sparkline/jquery.sparkline.min.js') }}"></script> -->

    <!-- Chart Circle -->
    <!-- <script src="{{ asset('app/js/plugin/chart-circle/circles.min.js') }}"></script> -->

    <!-- Datatables -->
    <script src="{{ asset('app/js/plugin/datatables/datatables.min.js') }}"></script>

    <!-- Bootstrap Notify -->
    <script src="{{ asset('app/js/plugin/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

    <!-- jQuery Vector Maps -->
    <!-- <script src="{{ asset('app/js/plugin/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('app/js/plugin/jqvmap/maps/jquery.vmap.world.js') }}"></script> -->

    <!-- Sweet Alert -->
    <script src="{{ asset('app/js/plugin/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Atlantis JS -->
    <script src="{{ asset('app/js/atlantis.min.js') }}"></script>

    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <!-- <script src="{{ asset('app/js/setting-demo.js') }}"></script> -->
    <!-- <script src="{{ asset('app/js/demo.js') }}"></script> -->
    <script>
        // Circles.create({
        //     id:'circles-1',
        //     radius:45,
        //     value:60,
        //     maxValue:100,
        //     width:7,
        //     text: 5,
        //     colors:['#f1f1f1', '#FF9E27'],
        //     duration:400,
        //     wrpClass:'circles-wrp',
        //     textClass:'circles-text',
        //     styleWrapper:true,
        //     styleText:true
        // })

        // Circles.create({
        //     id:'circles-2',
        //     radius:45,
        //     value:70,
        //     maxValue:100,
        //     width:7,
        //     text: 36,
        //     colors:['#f1f1f1', '#2BB930'],
        //     duration:400,
        //     wrpClass:'circles-wrp',
        //     textClass:'circles-text',
        //     styleWrapper:true,
        //     styleText:true
        // })

        // Circles.create({
        //     id:'circles-3',
        //     radius:45,
        //     value:40,
        //     maxValue:100,
        //     width:7,
        //     text: 12,
        //     colors:['#f1f1f1', '#F25961'],
        //     duration:400,
        //     wrpClass:'circles-wrp',
        //     textClass:'circles-text',
        //     styleWrapper:true,
        //     styleText:true
        // })

        // var totalIncomeChart = document.getElementById('totalIncomeChart').getContext('2d');

        // var mytotalIncomeChart = new Chart(totalIncomeChart, {
        //     type: 'bar',
        //     data: {
        //         labels: ["S", "M", "T", "W", "T", "F", "S", "S", "M", "T"],
        //         datasets : [{
        //             label: "Total Income",
        //             backgroundColor: '#ff9e27',
        //             borderColor: 'rgb(23, 125, 255)',
        //             data: [6, 4, 9, 5, 4, 6, 4, 3, 8, 10],
        //         }],
        //     },
        //     options: {
        //         responsive: true,
        //         maintainAspectRatio: false,
        //         legend: {
        //             display: false,
        //         },
        //         scales: {
        //             yAxes: [{
        //                 ticks: {
        //                     display: false //this will remove only the label
        //                 },
        //                 gridLines : {
        //                     drawBorder: false,
        //                     display : false
        //                 }
        //             }],
        //             xAxes : [ {
        //                 gridLines : {
        //                     drawBorder: false,
        //                     display : false
        //                 }
        //             }]
        //         },
        //     }
        // });

        // $('#lineChart').sparkline([105,103,123,100,95,105,115], {
        //     type: 'line',
        //     height: '70',
        //     width: '100%',
        //     lineWidth: '2',
        //     lineColor: '#ffa534',
        //     fillColor: 'rgba(255, 165, 52, .14)'
        // });
    </script>

    @stack('scripts')
</body>
</html>
