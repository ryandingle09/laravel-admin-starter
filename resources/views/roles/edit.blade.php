@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">User Roles</h2>
                        <h5 class="text-white op-7 mb-2">Edit User Role</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('role') }}" class="btn btn-white btn-border btn-round mr-2">Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{ route('role-update', [$data->id]) }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ (old('name')) ? old('name') : $data->name }}" autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="prefix" class="col-md-4 col-form-label text-md-right">Prefix</label>
                                    
                                    <div class="col-md-6">
                                        <input id="prefix" type="text" class="form-control @error('prefix') is-invalid @enderror" name="prefix" value="{{ (old('prefix')) ? old('prefix') : $data->prefix }}" autocomplete="prefix" autofocus>

                                        @error('prefix')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="description" class="col-md-4 col-form-label text-md-right">Description</label>

                                    <div class="col-md-6">
                                        <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" autocomplete="description">{{ (old('description')) ? old('description') : $data->description }}</textarea>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="modules" class="col-md-4 col-form-label text-md-right">Modules</label>

                                    <div class="col-md-6">
                                        <input type="hidden" class="form-control @error('modules') is-invalid @enderror">
                                        @foreach($modules as $m)
                                            @php
                                                $checked = '';
                                            @endphp

                                            @foreach($role_data as $rd)
                                                @if($rd->module_id == $m->id)
                                                    @php
                                                        $checked = 'checked="checked"';
                                                    @endphp
                                                @endif
                                            @endforeach
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="modules[]" id="modules" value="{{ $m->id }}" {{ $checked }}>
                                                <span class="form-check-sign">{{ $m->name }}</span>
                                            </label>
                                        </div>
                                        @endforeach

                                        @error('modules')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection