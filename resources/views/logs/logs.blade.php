@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Logs</h2>
                        <h5 class="text-white op-7 mb-2">User Logs</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-head-bg-success">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">User</th>
                                        <th scope="col">Action</th>
                                        <th scope="col">Module</th>
                                        <th scope="col" style="width: 15%">New Data</th>
                                        <th scope="col" style="width: 15%">Old Data</th>
                                        <th scope="col" style="width: 5%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($data) == 0)
                                    <tr>
                                        <td cols="7">No Data Found.</td>
                                    </tr>
                                    @else
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->user->email }}</td>
                                            <td>{{ $item->access->name }}</td>
                                            <td>{{ $item->module->name }}</td>
                                            <td>{{ $item->new_data }}</td>
                                            <td>{{ $item->old_data }}</td>
                                            <td>
                                                @if(Auth::user()->is_super_admin == 1)
                                                    <a href="{{ route('log-show', [$item->id]) }}"><i class="far fa-eye"></i></a>
                                                @else
                                                    @if($item->is_super_admin == 0)
                                                        @foreach(Auth::user()->user_access as $ua)
                                                            @if($ua->access->prefix == 'read')
                                                            <a href="{{ route('log-show', [$item->id]) }}"><i class="far fa-eye"></i></a>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
