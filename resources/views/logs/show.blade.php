@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">Logs</h2>
                        <h5 class="text-white op-7 mb-2">Showing log <b>{{ $data->user->email }}</b> details</h5>
                    </div>
                    
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('access') }}" class="btn btn-white btn-border btn-round mr-2">Back</a>
                        <a href="{{ route('access-edit', [$data->id]) }}" class="btn btn-white btn-border btn-round mr-2">Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">User:</label>

                                <div class="col-md-6">
                                <h3 style="text-decoration: underline">{{ $data->user->email }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Action:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ $data->access->name }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Module:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->module->name }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Data:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->new_data }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-md-4 col-form-label text-md-right">Old Data:</label>

                                <div class="col-md-6">
                                    <h3>{{ $data->old_data }} </h3>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="prefix" class="col-md-4 col-form-label text-md-right">Created At:</label>
                                
                                <div class="col-md-6">
                                    <h3 style="text-decoration: underline">{{ date('Y-m-d', strtotime($data->created_at)) }} </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
