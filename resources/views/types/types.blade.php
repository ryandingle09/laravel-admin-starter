@extends('layouts.app')

@section('content')
<div class="main-panel">
    <div class="content">
        <div class="panel-header bg-primary-gradient">
            <div class="page-inner py-5">
                <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="text-white pb-2 fw-bold">User Types</h2>
                        <h5 class="text-white op-7 mb-2">List of User Type</h5>
                    </div>
                    <div class="ml-md-auto py-2 py-md-0">
                        <a href="{{ route('type-create') }}" class="btn btn-white btn-border btn-round mr-2">Add User Type</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-inner mt--5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-head-bg-success">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Prefix</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Role</th>
                                        <th scope="col" style="width: 15%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($data) == 0)
                                    <tr>
                                        <td cols="6">No Data Found.</td>
                                    </tr>
                                    @else
                                        @foreach($data as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->prefix }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>{{ $item->role->name }}</td>
                                            <td>
                                                @if(Auth::user()->is_super_admin == 1)
                                                    <a href="{{ route('type-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
                                                    <a href="{{ route('type-edit', [$item->id]) }}"><i class="fas fa-pencil-alt"></i></a>&nbsp;
                                                    <a href="{{ route('type-destroy', [$item->id]) }}" class="delete" data-id="{{ $item->id }}"><i class="fas fa-trash-alt"></i></a>

                                                    <form id="form-{{ $item->id }}" action="{{ route('type-destroy', [$item->id]) }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                @else
                                                    @foreach(Auth::user()->user_access as $ua)
                                                        @if($ua->access->prefix == 'read')
                                                        <a href="{{ route('type-show', [$item->id]) }}"><i class="far fa-eye"></i></a>&nbsp;
                                                        
                                                        @elseif($ua->access->prefix == 'update')
                                                        <a href="{{ route('type-edit', [$item->id]) }}"><i class="fas fa-pencil-alt"></i></a>&nbsp;
                                                        

                                                        @elseif($ua->access->prefix == 'delete')
                                                        <a href="{{ route('type-destroy', [$item->id]) }}" class="delete" data-id="{{ $item->id }}"><i class="fas fa-trash-alt"></i></a>

                                                        <form id="form-{{ $item->id }}" action="{{ route('type-destroy', [$item->id]) }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    @if (session('status'))
        var content = {};
        var style = "withicon";
        content.message = "{{ session('status') }}";
        content.title = 'Notification';
        if (style == "withicon") {
            content.icon = 'fa fa-bell';
        } else {
            content.icon = 'none';
        }

        $.notify(content,{
            type: 'success',
            placement: {
                from: 'top',
                align: 'right'
            },
            time: 1000,
            delay: 0,
        });
    @endif

    $('.delete').click(function(e) {
        e.preventDefault();

        var id = $(this).data('id');

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            buttons:{
                confirm: {
                    text : 'Yes, delete it!',
                    className : 'btn btn-success'
                },
                cancel: {
                    visible: true,
                    className: 'btn btn-danger'
                }
            }
        }).then((Delete) => {
            if (Delete) {
                $('#form-'+ id).submit();
            } else {
                swal.close();
            }
        });
    });
</script>
@endpush
