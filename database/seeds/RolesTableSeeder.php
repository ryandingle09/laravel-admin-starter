<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Admin Role',
                'prefix' => 'admin',
                'description' => 'Admin user Type',
            ],
            [
                'name' => 'Encoder',
                'prefix' => 'encoder',
                'description' => 'Encoder user Type',
            ]
        ]);
    }
}
