<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('access')->insert([
            [
                'name' => 'create',
                'prefix' => 'create',
                'description' => 'Create Access Level',
            ],
            [
                'name' => 'read',
                'prefix' => 'read',
                'description' => 'Read Access Level',
            ],
            [
                'name' => 'update',
                'prefix' => 'update',
                'description' => 'Update Access Level',
            ],
            [
                'name' => 'delete',
                'prefix' => 'delete',
                'description' => 'Delete Access Level',
            ]
        ]);
    }
}
