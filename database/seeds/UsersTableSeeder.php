<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'email' => 'superadmin@gmail.com',
                'password' => Hash::make('p@ssw0rd'),
                'is_super_admin' => 1,
                'status' => 1,
                'created_by' => 1,
                'updated_by' => 1
            ]
        );
    }
}
