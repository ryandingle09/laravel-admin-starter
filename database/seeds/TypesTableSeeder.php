<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'name' => 'CEO',
                'prefix' => 'ceo',
                'description' => 'CEO user Type',
                'role_id' => 1
            ],
            [
                'name' => 'Manager',
                'prefix' => 'manager',
                'description' => 'Manager user Type',
                'role_id' => 2
            ]
        ]);
    }
}
