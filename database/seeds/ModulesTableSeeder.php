<?php

use Illuminate\Database\Seeder;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'name' => 'Users',
                'prefix' => 'user',
                'description' => 'User Module',
                'icon' => 'fas fa-users',
                'order' => 1
            ],
            [
                'name' => 'User Types',
                'prefix' => 'type',
                'description' => 'User Type Module',
                'icon' => 'fas fa-users-cog',
                'order' => 2
            ],
            [
                'name' => 'Roles',
                'prefix' => 'role',
                'description' => 'Roles Module',
                'icon' => 'fas fa-user-shield',
                'order' => 3
            ],
            [
                'name' => 'Access Rights',
                'prefix' => 'access',
                'description' => 'Access Rights Module',
                'icon' => 'fas fa-shield-alt',
                'order' => 4
            ],
            [
                'name' => 'Modules',
                'prefix' => 'module',
                'description' => 'Modules Module',
                'icon' => 'fas fa-folder',
                'order' => 5
            ],
            [
                'name' => 'Logs',
                'prefix' => 'log',
                'description' => 'Log/Audit Trail Module',
                'icon' => 'far fa-eye',
                'order' => 6
            ]
        ]);
    }
}
